#+TITLE: Evolutionary Optimization of Analog Amplifiers

** Abstract
 This thesis demonstrates the capabilities of evolutionary algorithms, namely
  evolution strategies, in the design of analog amplifiers. We propose various
  methods for evaluating the amplifiers and perform multiple experiments, the
  results of which we use to determine the most optimal parameters for the
  evolution strategies. The goal is to optimize the values of the components of
  single and two-stage common-emitter amplifiers. The resulting tool is able to
  find the values so that the amplifier has the desired gain. We integrate the
  ngSPICE simulator to simulate the circuits and evaluate their properties.
  
-----

The thesis can be found [[file:thesis/thesis.pdf][here]].

If you wish to optimize the amplifiers yourself, the compilation process is
[[file:INSTALL][here]].

-----

The following image illustrates the evolution across 50 generations. The red
wave represents the desired output signal. The green wave represents the output
of the best amplifier in the current population.

#+CAPTION: An example of the evolution
#+NAME:   fig:evolution
[[./thesis/figures/evolutionCourse/evolution.gif]]

